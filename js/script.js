/* Як можна оголосити змінну у Javascript?
Змінна оголошується за допомогою слів var, let, const.
var використовувалось до 2015 року, наразі здебільшого використовують let та const.
const використовується для оголошення змінної, значення якої в майбутньому не буде змінюватись, 
let – для оголошення змінної, значення якої в майбутньому буде змінюватись.

У чому різниця між функцією prompt та функцією confirm?
Функція confirm виводить модальне вікно користувачу з повідомленням та двома кнопками - OK та Cancel.
Введення текстової відповіді неможливе, результатом є true якщо користувач погодиться і натисне OK 
та false якщо Cancel.
Навідміну від confirm promt викликає модальне вікно, в якому міститься поле 
для введення користувачем тексту для відповіді на повідомлення, зазначене у вікні.  
Результатом буде ця відповідь якщо користувач напише текст і натисне OK та null - якщо натисне Cancel 
чи Esc.

Що таке неявне перетворення типів? Наведіть один приклад.
Неявне перетворення це автоматичне перетворення значень різних типів даних.
Наприклад: коли відбувається складення числового значення та рядкового.
Результатом price = 11 + "5" буде рядкове значення 115, а не 16, 
оскільки відбудеться неявне перетворення числа 11 в рядок і об'єднання
з рядковим значенням 5.

      */
const name = 'Maksym';
let admin = name;
console.log(admin);

let days = 7;
days = String(7 * 24 * 60 * 60) + ' ' + 'seconds';
console.log(days);

let age = +prompt('How old are you?');
console.log(age);